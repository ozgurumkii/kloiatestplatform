﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Model.Dtos.Interfaces
{
    public interface IDto
    {
        int Id { get; set; }
    }
}
