﻿using AutoMapper;
using KloiaTestPlatform.Entities.Concrete;
using KloiaTestPlatform.Model.Dtos.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Model.Dtos.Mapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            /* Entity To Dto --- Dto To Entity */
            CreateMap<Article, ArticleDto>().ReverseMap();
            CreateMap<Review, ReviewDto>().ReverseMap();
        }
    }
}
