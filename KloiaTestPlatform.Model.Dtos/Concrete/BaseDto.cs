﻿using KloiaTestPlatform.Model.Dtos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Model.Dtos.Concrete
{
    public class BaseDto : IDto
    {
        public int Id { get; set; }
    }
}
