﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Model.Dtos.Concrete
{
    public class ReviewDto : BaseDto
    {
        public int ArticleId { get; set; }
        public string Reviewer { get; set; }
        public string ReviewContent { get; set; }
        public ArticleDto Article { get; set; }
    }
}
