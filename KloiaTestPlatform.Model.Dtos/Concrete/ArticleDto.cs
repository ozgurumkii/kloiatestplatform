﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Model.Dtos.Concrete
{
    public class ArticleDto : BaseDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ArticleContent { get; set; }
        public DateTime PublishDate { get; set; }
        public int? StarCount { get; set; }
        public IEnumerable<ReviewDto> Reviews { get; set; }
    }
}
