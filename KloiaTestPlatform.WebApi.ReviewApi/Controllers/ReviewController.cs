﻿using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Services.ReviewService.Interfaces;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KloiaTestPlatform.WebApi.ReviewApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet]
        [EnableQuery]
        public IEnumerable<ReviewDto> Get()
        {
            return _reviewService.GetAll();
        }

        [HttpGet("{id}")]
        public ReviewDto Get(int id)
        {
            return _reviewService.GetSingle(id);
        }

        [HttpPost("Create")]
        public IActionResult Create(ReviewDto dto)
        {
            return Ok(_reviewService.Create(dto));
        }

        [HttpPost("Update")]
        public IActionResult Update(ReviewDto dto)
        {
            return Ok(_reviewService.Update(dto));
        }

        [HttpGet("Delete/{id}")]
        public IActionResult Delete(int id)
        {
            return Ok(_reviewService.Delete(id));
        }
    }
}
