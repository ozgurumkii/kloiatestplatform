#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["KloiaTestPlatform.WebApi.ReviewApi/KloiaTestPlatform.WebApi.ReviewApi.csproj", "KloiaTestPlatform.WebApi.ReviewApi/"]
COPY ["KloiaTestPlatform.Services.Review/KloiaTestPlatform.Services.ReviewService.csproj", "KloiaTestPlatform.Services.Review/"]
COPY ["KloiaTestPlatform.Services/KloiaTestPlatform.Services.csproj", "KloiaTestPlatform.Services/"]
COPY ["KloiaTestPlatform.Model.Dtos/KloiaTestPlatform.Model.Dtos.csproj", "KloiaTestPlatform.Model.Dtos/"]
COPY ["KloiaTestPlatform.Entities/KloiaTestPlatform.Entities.csproj", "KloiaTestPlatform.Entities/"]
COPY ["KloiaTestPlatform.Domain/KloiaTestPlatform.Domain.csproj", "KloiaTestPlatform.Domain/"]
RUN dotnet restore "KloiaTestPlatform.WebApi.ReviewApi/KloiaTestPlatform.WebApi.ReviewApi.csproj"
COPY . .
WORKDIR "/src/KloiaTestPlatform.WebApi.ReviewApi"
RUN dotnet build "KloiaTestPlatform.WebApi.ReviewApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "KloiaTestPlatform.WebApi.ReviewApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "KloiaTestPlatform.WebApi.ReviewApi.dll"]