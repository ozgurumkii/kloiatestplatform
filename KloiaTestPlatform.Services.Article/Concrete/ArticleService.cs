﻿using AutoMapper;
using KloiaTestPlatform.Domain.Repository;
using KloiaTestPlatform.Domain.UnitOfWork;
using KloiaTestPlatform.Entities.Concrete;
using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Services.ArticleService.Interfaces;
using KloiaTestPlatform.Services.Concrete;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Services.ArticleService.Concrete
{
    public class ArticleService : IArticleService
    {
        private readonly IEntityRepository<Article> _articleRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ArticleService(IEntityRepository<Article> articleRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _articleRepository = articleRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /* Crud Methods */
        public OperationResult<ArticleDto> Create(ArticleDto entityDto)
        {
            try
            {
                if (entityDto == null) { throw new ArgumentNullException("entityDto parameter is null!"); }
                var entity = _mapper.Map<Article>(entityDto);
                entity.PublishDate = DateTime.Now;
                _articleRepository.Add(entity);
                _unitOfWork.Commit();
                entity = _articleRepository.GetSingle(entity.Id);
                entityDto = _mapper.Map<ArticleDto>(entity);
                return new OperationResult<ArticleDto>(OperationResultType.SUCCESS, "Created", "Success") { EntityDto = entityDto };
            }
            catch (Exception ex)
            {
                return new OperationResult<ArticleDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = entityDto };
            }
        }

        public OperationResult<ArticleDto> Delete(int id)
        {
            try
            {
                var existing = _articleRepository.GetAllAsQueryable().Include(x => x.Review).FirstOrDefault(x => x.Id == id);
                if (existing != null)
                {
                    if (existing.Review.Count > 0)
                    {
                        return new OperationResult<ArticleDto>(OperationResultType.WARNING, "There are reviews under that Article", "Warning") { EntityDto = null };
                    }
                    else
                    {
                        _articleRepository.Delete(existing);
                        _unitOfWork.Commit();
                        var dto = Entity_ToDto(existing);
                        return new OperationResult<ArticleDto>(OperationResultType.SUCCESS, "Deleted", "Success") { EntityDto = dto };
                    }
                }
                return new OperationResult<ArticleDto>(OperationResultType.WARNING, "Article not found", "Warning") { EntityDto = null };
            }
            catch (Exception ex)
            {
                return new OperationResult<ArticleDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = null };
            }
        }

        public IEnumerable<ArticleDto> GetAll()
        {
            return _articleRepository.GetAll().Select(x => _mapper.Map<ArticleDto>(x));
        }

        public ArticleDto GetSingle(int id)
        {
            return _mapper.Map<ArticleDto>(_articleRepository.GetSingle(id));
        }

        public OperationResult<ArticleDto> Update(ArticleDto entityDto)
        {
            try
            {
                var existing = _articleRepository.GetSingle(entityDto.Id);
                if (existing != null)
                {
                    existing.Title = entityDto.Title;
                    existing.Author = entityDto.Author;
                    existing.ArticleContent = entityDto.ArticleContent;
                    existing.StarCount = entityDto.StarCount;
                    _articleRepository.Edit(existing);
                    _unitOfWork.Commit();
                    return new OperationResult<ArticleDto>(OperationResultType.SUCCESS, "Updated", "Success") { EntityDto = entityDto };
                }
                return new OperationResult<ArticleDto>(OperationResultType.WARNING, "Not Found", "Warning") { EntityDto = entityDto };
            }
            catch (Exception ex)
            {
                return new OperationResult<ArticleDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = entityDto };
            }
        }

        /* Dto To Entity, Entity To Dto */
        protected ArticleDto Entity_ToDto(Article entity)
        {
            if (entity != null)
            {
                var dto = _mapper.Map<ArticleDto>(entity);
                if (entity.Review != null)
                {
                    dto.Reviews = entity.Review.Select(x => _mapper.Map<ReviewDto>(x));
                }
                return dto;
            }
            else
                return null;
        }
    }
}
