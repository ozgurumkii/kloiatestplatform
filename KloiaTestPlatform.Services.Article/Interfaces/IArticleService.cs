﻿using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Services.ArticleService.Interfaces
{
    public interface IArticleService : IEntityService<ArticleDto>
    {
    }
}
