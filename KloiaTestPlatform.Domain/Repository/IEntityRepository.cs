﻿using KloiaTestPlatform.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Domain.Repository
{
    public interface IEntityRepository<T> where T : class, IEntityBase, new()
    {
        IEnumerable<T> All { get; }
        IQueryable<T> GetAllAsQueryable();
        IQueryable<T> GetAllAsQueryable(bool useNoTracking);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(bool useNoTracking);
        IEnumerable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate);
        T GetSingle(int id);
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
    }
}
