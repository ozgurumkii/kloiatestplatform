﻿using System;
using System.Collections.Generic;

namespace KloiaTestPlatform.Entities.Concrete
{
    public partial class Review : EntityBase
    {
        public int ArticleId { get; set; }
        public string Reviewer { get; set; }
        public string ReviewContent { get; set; }

        public virtual Article Article { get; set; }
    }
}