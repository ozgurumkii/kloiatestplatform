﻿using System;
using System.Collections.Generic;

namespace KloiaTestPlatform.Entities.Concrete
{
    public partial class Article : EntityBase
    {
        public Article()
        {
            Review = new HashSet<Review>();
        }

        public string Title { get; set; }
        public string Author { get; set; }
        public string ArticleContent { get; set; }
        public DateTime PublishDate { get; set; }
        public int? StarCount { get; set; }

        public virtual ICollection<Review> Review { get; set; }
    }
}