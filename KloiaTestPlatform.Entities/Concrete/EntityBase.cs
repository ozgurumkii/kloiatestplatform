﻿using KloiaTestPlatform.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Entities.Concrete
{
    public class EntityBase : IEntityBase
    {
        public int Id { get; set; }
    }
}
