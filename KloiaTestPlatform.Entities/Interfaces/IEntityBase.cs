﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Entities.Interfaces
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}
