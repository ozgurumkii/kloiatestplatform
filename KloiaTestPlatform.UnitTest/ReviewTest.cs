﻿using AutoMapper;
using KloiaTestPlatform.Domain.Repository;
using KloiaTestPlatform.Domain.UnitOfWork;
using KloiaTestPlatform.Entities.Concrete;
using KloiaTestPlatform.Model.Dtos.Mapper;
using KloiaTestPlatform.Services.ReviewService.Concrete;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.UnitTest
{
    public class ReviewTest
    {
        private List<Review> _reviewList;

        private Mock<IEntityRepository<Review>> _reviewRepository;
        private Mock<IEntityRepository<Article>> _articleRepository;
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;

        private ReviewService _reviewService;

        [SetUp]
        public void Setup()
        {
            GetReviews();
            // Map config is binding
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapping());
            });

            // Instances
            _unitOfWork = new Mock<IUnitOfWork>();
            _mapper = mappingConfig.CreateMapper();
            _reviewRepository = new Mock<IEntityRepository<Review>>();
            _articleRepository = new Mock<IEntityRepository<Article>>();

            // Setup And Return
            _reviewRepository.Setup(x => x.GetAll()).Returns(GetReviews());
            _reviewRepository.Setup(x => x.GetSingle(3)).Returns(GetReviews().FirstOrDefault(s => s.Id == 3));

            // Mock Service Instance
            _reviewService = new ReviewService(_reviewRepository.Object, _articleRepository.Object, _unitOfWork.Object, _mapper);
        }

        [Test]
        public void Review_GetAll_Test()
        {
            Assert.AreEqual(4, _reviewService.GetAll().Count());
        }

        [Test]
        public void Review_GetSingle_Test()
        {
            Assert.AreEqual("Deneme3", _reviewService.GetSingle(3).Reviewer);
        }

        public IEnumerable<Review> GetReviews()
        {
            _reviewList = new List<Review>()
            {
                new Review(){ Id=1, Reviewer="Deneme1", ReviewContent="Bu bir denemedir", Article=null, ArticleId=1 },
                new Review(){ Id=2, Reviewer="Deneme2", ReviewContent="Bu bir denemedir", Article=null, ArticleId=1 },
                new Review(){ Id=3, Reviewer="Deneme3", ReviewContent="Bu bir denemedir", Article=null, ArticleId=1 },
                new Review(){ Id=4, Reviewer="Deneme4", ReviewContent="Bu bir denemedir", Article=null, ArticleId=1 }
            };

            return _reviewList;
        }
    }
}
