﻿using AutoMapper;
using KloiaTestPlatform.Domain.Repository;
using KloiaTestPlatform.Domain.UnitOfWork;
using KloiaTestPlatform.Entities.Concrete;
using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Model.Dtos.Mapper;
using KloiaTestPlatform.Services.ArticleService.Concrete;
using KloiaTestPlatform.Services.Concrete;
using Moq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace KloiaTestPlatform.UnitTest
{
    public class ArticleTest
    {
        private List<Article> _articleList;

        private Mock<IEntityRepository<Article>> _articleRepository;
        private Mock<IUnitOfWork> _unitOfWork;
        private IMapper _mapper;

        private ArticleService _articleService;

        [SetUp]
        public void Setup()
        {
            GetArticles();
            // Map config is binding
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapping());
            });

            // Instances
            _unitOfWork = new Mock<IUnitOfWork>();
            _mapper = mappingConfig.CreateMapper();
            _articleRepository = new Mock<IEntityRepository<Article>>();

            // Setup And Return
            _articleRepository.Setup(x => x.GetAll()).Returns(GetArticles());
            _articleRepository.Setup(x => x.GetSingle(3)).Returns(GetArticles().FirstOrDefault(s => s.Id == 3));

            // Mock Service Instance
            _articleService = new ArticleService(_articleRepository.Object, _unitOfWork.Object, _mapper);
        }

        [Test]
        public void Article_GetAll_Test()
        {
            Assert.AreEqual(5, _articleService.GetAll().Count());
        }

        [Test]
        public void Article_GetSingle_Test()
        {
            Assert.AreEqual("Deneme3", _articleService.GetSingle(3).Title);
        }

        public IEnumerable<Article> GetArticles()
        {
            _articleList = new List<Article>()
            {
                new Article(){ Id=1, Title="Deneme1", ArticleContent="Bu bir denemedir", Author="Özgür", PublishDate=DateTime.Now, StarCount=4, Review=null },
                new Article(){ Id=2, Title="Deneme2", ArticleContent="Bu bir denemedir", Author="Özgür", PublishDate=DateTime.Now, StarCount=4, Review=null },
                new Article(){ Id=3, Title="Deneme3", ArticleContent="Bu bir denemedir", Author="Özgür", PublishDate=DateTime.Now, StarCount=1, Review=null },
                new Article(){ Id=4, Title="Deneme4", ArticleContent="Bu bir denemedir", Author="Özgür", PublishDate=DateTime.Now, StarCount=4, Review=null },
                new Article(){ Id=5, Title="Deneme5", ArticleContent="Bu bir denemedir", Author="Özgür", PublishDate=DateTime.Now, StarCount=4, Review=null }
            };

            return _articleList;
        }
    }
}
