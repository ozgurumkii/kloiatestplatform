﻿using AutoMapper;
using KloiaTestPlatform.Domain.Repository;
using KloiaTestPlatform.Domain.UnitOfWork;
using KloiaTestPlatform.Entities.Concrete;
using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Services.Concrete;
using KloiaTestPlatform.Services.ReviewService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Services.ReviewService.Concrete
{
    public class ReviewService : IReviewService
    {
        private readonly IEntityRepository<Review> _reviewRepository;
        private readonly IEntityRepository<Article> _articleRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReviewService(IEntityRepository<Review> reviewRepository, IEntityRepository<Article> articleRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _reviewRepository = reviewRepository;
            _articleRepository = articleRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /* Crud Methods */
        public OperationResult<ReviewDto> Create(ReviewDto entityDto)
        {
            try
            {
                if (entityDto == null) { throw new ArgumentNullException("entityDto parameter is null!"); }
                var article = _articleRepository.GetSingle(entityDto.ArticleId);
                if (article != null)
                {
                    var entity = _mapper.Map<Review>(entityDto);
                    _reviewRepository.Add(entity);
                    _unitOfWork.Commit();
                    entity = _reviewRepository.GetSingle(entity.Id);
                    entityDto = _mapper.Map<ReviewDto>(entity);
                    return new OperationResult<ReviewDto>(OperationResultType.SUCCESS, "Created", "Success") { EntityDto = entityDto };
                }
                return new OperationResult<ReviewDto>(OperationResultType.WARNING, "Article not found", "Warning") { EntityDto = entityDto };
            }
            catch (Exception ex)
            {
                return new OperationResult<ReviewDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = entityDto };
            }
        }

        public OperationResult<ReviewDto> Delete(int id)
        {
            try
            {
                var existing = _reviewRepository.GetSingle(id);
                if (existing != null)
                {
                    _reviewRepository.Delete(existing);
                    _unitOfWork.Commit();
                    var dto = _mapper.Map<ReviewDto>(existing);
                    return new OperationResult<ReviewDto>(OperationResultType.SUCCESS, "Deleted", "Success") { EntityDto = dto };
                }
                return new OperationResult<ReviewDto>(OperationResultType.WARNING, "Review not found", "Warning") { EntityDto = null };
            }
            catch (Exception ex)
            {
                return new OperationResult<ReviewDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = null };
            }
        }

        public IEnumerable<ReviewDto> GetAll()
        {
            return _reviewRepository.GetAll().Select(x => _mapper.Map<ReviewDto>(x));
        }

        public ReviewDto GetSingle(int id)
        {
            return _mapper.Map<ReviewDto>(_reviewRepository.GetSingle(id));
        }

        public OperationResult<ReviewDto> Update(ReviewDto entityDto)
        {
            try
            {
                var existing = _reviewRepository.GetSingle(entityDto.Id);
                if (existing != null)
                {
                    existing.Reviewer = entityDto.Reviewer;
                    existing.ReviewContent = entityDto.ReviewContent;
                    _reviewRepository.Edit(existing);
                    _unitOfWork.Commit();
                    return new OperationResult<ReviewDto>(OperationResultType.SUCCESS, "Updated", "Success") { EntityDto = entityDto };
                }
                return new OperationResult<ReviewDto>(OperationResultType.WARNING, "Not Found", "Warning") { EntityDto = entityDto };
            }
            catch (Exception ex)
            {
                return new OperationResult<ReviewDto>(OperationResultType.ERROR, ex.Message, "Error") { EntityDto = entityDto };
            }
        }
    }
}
