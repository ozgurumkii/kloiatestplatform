﻿using KloiaTestPlatform.Services.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KloiaTestPlatform.Services.Interfaces
{
    public interface IEntityService<TDto> : IService where TDto : class, new()
    {
        IEnumerable<TDto> GetAll();
        TDto GetSingle(int id);
        OperationResult<TDto> Create(TDto entityDto);
        OperationResult<TDto> Update(TDto entityDto);
        OperationResult<TDto> Delete(int id);
    }
}
