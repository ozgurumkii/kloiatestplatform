using KloiaTestPlatform.Domain.EFDbContext;
using KloiaTestPlatform.Domain.Repository;
using KloiaTestPlatform.Domain.UnitOfWork;
using KloiaTestPlatform.Model.Dtos.Mapper;
using KloiaTestPlatform.Services.ArticleService.Concrete;
using KloiaTestPlatform.Services.ArticleService.Interfaces;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KloiaTestPlatform.WebApi.ArticleApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddNewtonsoftJson();

            services.AddOData();

            services.AddAutoMapper(typeof(AutoMapping));

            services.AddDbContext<EntitiesContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ConnectionString")));

            // Dependency Injection
            services.AddScoped<DbContext, EntitiesContext>();
            services.AddScoped(typeof(IEntityRepository<>), typeof(EntityRepository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IArticleService, ArticleService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.EnableDependencyInjection();
                endpoints.Select().Filter().OrderBy().Count().MaxTop(100).Expand();

                endpoints.MapControllers();
            });
        }
    }
}
