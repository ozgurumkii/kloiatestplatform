﻿using KloiaTestPlatform.Model.Dtos.Concrete;
using KloiaTestPlatform.Services.ArticleService.Interfaces;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KloiaTestPlatform.WebApi.ArticleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [HttpGet]
        [EnableQuery]
        public IEnumerable<ArticleDto> Get()
        {
            return _articleService.GetAll();
        }

        [HttpGet("{id}")]
        public ArticleDto Get(int id)
        {
            return _articleService.GetSingle(id);
        }

        [HttpPost("Create")]
        public IActionResult Create(ArticleDto dto)
        {
            return Ok(_articleService.Create(dto));
        }

        [HttpPost("Update")]
        public IActionResult Update(ArticleDto dto)
        {
            return Ok(_articleService.Update(dto));
        }

        [HttpGet("Delete/{id}")]
        public IActionResult Delete(int id)
        {
            return Ok(_articleService.Delete(id));
        }
    }
}
